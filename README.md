# AgroData

### Objetivo do Projeto

Hoje os dados são o novo petróleo de mundo, através dos derivados que podem ser gerados a partir dele, há a possibilidade de criar novos negócios e soluções.
A Agrodata é um projeto que visa entregar valor ao produtor rural com base nos dados que estão disponibilizados na internet. A equipe coletará os dados no Censo agro 2017, e no site do CAR.

**O projeto consiste em 3 etapas:**

- Primeira etapa, planejar os dados que serão coletados;
- Segunda etapa, fazer o KDD;
- Encontrar pessoas que possam utilizar nossa solução;

Com base nos testes do MVP, iremos avaliar a continuidade do projeto. Nesse caso, precisamos de dados do publico ideal para utilizar nosso sistema.
